FROM codercom/code-server:3.5.0

RUN curl -L https://golang.org/dl/go1.15.1.linux-amd64.tar.gz -o go.tgz && \
    sudo tar -C /usr/local -xzf go.tgz && \
    curl -sL https://github.com/gohugoio/hugo/releases/download/v0.74.3/hugo_0.74.3_Linux-64bit.tar.gz -o hugo.tgz && \
    sudo tar -C /usr/local/bin -xzf hugo.tgz && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.19.0/bin/linux/amd64/kubectl &&\
    chmod +x ./kubectl && sudo mv ./kubectl /usr/local/bin/kubectl && \
    curl -L https://get.helm.sh/helm-v3.3.1-linux-amd64.tar.gz -o helm.tgz && \
    tar -zxvf helm.tgz && \
    sudo mv linux-amd64/helm /usr/local/bin/helm && \
    helm repo add stable https://kubernetes-charts.storage.googleapis.com/ && \
    echo "PATH=/usr/local/go/bin:\$PATH" >> /home/coder/.bashrc && \
    sudo mkdir /tmp_coder && \
    sudo chown 1000:1000 /tmp_coder && \
    cp -R /home/coder /tmp_coder

EXPOSE 8080
EXPOSE 1313
USER coder
WORKDIR /home/coder
ENTRYPOINT ["dumb-init", "fixuid", "-q", "/usr/bin/code-server", "--bind-addr", "0.0.0.0:8080", "."]

