# Code Server Playground 
Original concept from https://gitlab.com/mterhar/code-server-buildtools

Very much a WIP so use at your own risk.

## Running
Could use skaffold to do this maybe...

Otherwise
Change the ${PROJECT_ID} to your project

```
kubectl apply -f ./kubernetes-manifests/*
kubectl port-forward $(kubectl get pods -l app=code-server -o jsonpath='{.items[0].metadata.name}') 8080:8080
```
Open up [localhost:8080](localhost:8080)
Password is: plane-reusteez-octo


